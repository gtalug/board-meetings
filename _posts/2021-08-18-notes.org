---
layout: post
title: "Notes"
published: true
---

* 18 Aug 2021 at 7:30pm

** Location
 - Remote based on COVID-19
 - Zoom (to be mailed on Ops mailng list)

** Attendees
 - Present
    - Alan Heighway (board member)
    - Scott Sullivan (board member)
    - Alex Volkov (board member)
    - Gordon Chilcott
    - Evan Leibovitch
    - Hugh Redelmeier

 - Absent
    - Warren McPherson (board member) w/ Notice


** Agenda Topics
 - Speaker Search
 - Update Bank Information
   - Add new Signing Auths
   - Remove old Siging Auths
   - Update Address (Scott is Moving)
 - Confirm Date and Time of Next Meeting

** Items from the previous meetings Mintues.
 - [] Secretray Handbook - Scott 
    - Not Done
 - [] Ensuring at least three people are sharing credentails via Lastpass - Alan.
   - Have not received and credentials to share. (Waiting on Alex)
 - [] Publish the a list of services in LastPass - Alex
   - Not Done
 - [x] Write Reimbursements check for Alex - Scott
 - [] On-board Scott on current meeting announce process. - Alex
   - Not Done, no longer relevant.
 - [] On-board Hugh for server admin. - Alex
   - Hugh feels there are still things he needs.
 - [] Schedule Techinical Infra Review Meeting - Scott
   - Not Done
 - [] Update Corporate records / continunce / Form 1 - Evan and Scott
   - Not Done

** Mintues
 - Speaker Search
   - Alan: Sent a follow to one prospect, but haven't heard anything.
   - Great work snagging Alyssa. 
   - Evan: Can we leverage our contacts at mozilla to talk on the Open Source Archetypes?
    - https://blog.mozilla.org/wp-content/uploads/2018/05/MZOTS_OS_Archetypes_report_ext_scr.pdf
   - Hugh: Can we reach out ot the other groups for them for resources? Or just to promote themselves.
   - Scott: I used to do just this back when physical meetings happened. I'd snag speakers for second runs of talks they did at those other groups.
   - Scott: Our video production pipeline (Major thanks Alex!) is a real value add for speaker to come do and encore at GTALUG
   - We have a lot of middle ground, double bills and lighten talk sets. So we don't always have to find hour long talks.
   - Alan is currently chasing 2 leads, but we have nothing landed yet.
   - Scott: Folks need to be CC'ing the speakers alias, and handing off potentials for the coordinator to follow up on..
 - Update Bank Information
   - Add new Signing Auths
   - Remove old Siging Auths
   - Update Address (Scott is Moving)
     - Alan: Appointment is currently being arranged.
     - Alan: It is likely we'll also need Stewart.
     - Scott: We also them to fix spelling mistake in our name on the statements.
  - Confirm Date and Time of Next Meeting
    - Wed Sept 22nd, 7:30pm

- Software Freedom Day is Saturday Sept 18th.

- Meetup.com - Evan
  - Have we tried it before?
  - Alex: Yes, we did a pilot, and the results proved not worth the cost.
  - Evan: We might be able to get a price break for being a non-profit.

- TechSoup - Evan
  - Alan: Collects company donations (of services, deals, etc) and vets legit recipents and doles them out.
  - Evan: Some of the benifits can be passed on further to card carrying members.

- Google.org - Evan
  - Evan: Expited access for non-profit reduced costs / free Google App services.
  - Evan: A Google AdWords budget
  - https://www.google.com/nonprofits/resources/product-help/

- Reminder: October is AGM and Board Elections - Gordon
  - Gordon: We need to be proactive about finding fresh blood.

** TODO
 - [] Ensuring at least three people are sharing credentails via Lastpass - Alan.
 - [] Publish the a list of services in LastPass - Alex
 - [] On-board Hugh for server admin. - Alex
 - [] Schedule Techinical Infra Review Meeting - Scott
 - [] Update Corporate records / continunce / Form 1 - Evan and Scott
 - [] Establish a TechSoup account. - Evan
 - [] Establish a Google.org application. - Evan
