---
layout: post
title: "Agenda"
published: true
---

* 20 April 2020 at 7:30pm

** Location
 - Remote based on COVID-19
 - Zoom

** Agenda Topics
 - Timing/Location of next Ops meeting
   - Look at Jitsi/Zoom/Hangouts again
 - COVID-19 question
   - we are a discretionary meeting
 - Upcoming Meetings
 - ICANN matters
 - Write next month's agenda
   - Chris to do this offline

** ToDo List
 - Done :: Chris to generate a draft on upcoming meetings versus COVID-19
 - TODO Gord :: see who we could ask for a second Ryerson access card
 - TODO :: Gord and Chris go to the bank
 - TODO Alex :: checklist for QandA
 - TODO Alex :: present CoC proposals at Feb board meeting, violation reporting formality
