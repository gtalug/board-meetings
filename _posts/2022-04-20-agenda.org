---
layout: post
title: "Agenda"
published: true
---

* 20 April 2022 at 7:30pm

** Location

  Remote based on COVID-19 using LPI's Big Blue Button
  https://blue.lpi.org/b/eva-zjc-gjy-kgl

** Agenda Topics

  Items from the previous meetings Mintues.
  New Business
  Speaker Search
  Confirm Date and Time of next board meeting
 
***  Items from the previous meetings Minutes.

  - [] Ensuring at least three people are sharing credentails via Lastpass - Alan.    
  - [] Publish the a list of services in LastPass - Alex
  - [] Update Corporate records / continunce / Form 1 - Evan and Alan
  - [] Offline Discussion with Alex and Hugh on Google service
  - [] Create Google accounts for the board members - Evan
  - [] Document the current notification process for general and board meetings - Alex
  - [] Report on DNS and Hosting Status for next meeting - Alex
  - [] Document board position - Ops Tasks - Everyone
  - [] Check up on complition of of board position documents - Alan
  - [] Follow-up with IDRC again (Speaker Topic) - Alan
  - [] Even and Alan to work on Goolgle for Non-profit
  - [] CIRA Speaker
    
*** New Business
    

**** Speaker Search


**** Confirm Date and Time of Next General Meeting
      Tuesday May 10th, at 7:30. Big Blue Button

**** Confirm Date and Time of next Board Meeting
      Wednesday May 18th, at 7:30. Big Blue Button

