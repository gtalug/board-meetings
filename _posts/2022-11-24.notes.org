
---
layout: post
title: "notes"
published: true
---

* 24 September 2022 at 7:30pm

** Location

  Remote based on COVID-19 using LPI's Big Blue Button
  https://blue.lpi.org/b/eva-zjc-gjy-kgl


** Attendees
 - Present
    - Alan Heighway (Board Member)
    - Evan Leibovitch (Board Member)
    - Warren McPherson (Board Member)
        
 - Absent
    - Gordon Chilcott (OPs Team)
    - Alex Volkov (Board Member)
    - David Collyer-Brown (Board Member)
    - Hugh Redelmeier (Ops Team) 
    - Gordon Chilcott (OPs Team)

** Agenda Topics

  December AGM
  Items from the previous meetings Mintues.
  New Business
  Speakers / Speaker Search
  Confirm Date and Time of next board meeting
 
***  Items from the previous meetings Minutes.

  - [] Ensuring at least three people are sharing credentials via Lastpass - Alan
  - [] Publish the a list of services in LastPass - Alex
  - [] Update Corporate records / continuance / Form 1 - Evan and Alan
  - [] Discuss Google utilities with Tech Team - Evan, Alan, Ops Team
  - [] Document board position - Ops Tasks - Everyone
  - [] Check up on completion of of board position documents - Alan
  - [] Follow up with general group on Google ad words and help with them - Alan
  - [] Even and Alan to work on Google for Non-profit
  - [X] Contact David Collyer-Brown about Board - Evan
  - [] Check Mr Meseeks for when it's 'suppose' to send out reminders - Hugh
      
*** New Business
  - [] In person meetings at the university for September?
  - [] Speak with Toronto Metropolitan University about in person meetings - Evan

**** Speaker Search (Topics and possible presenters)
    - Marine Navigtion via Raspberry Pi - Colen McGregor - September - Confirmed
    - Mike Hoye - October - Confirmed
    - Futur Topic - Warren - ?
    - Google Ad Words - Use and value to the group
    - Don from Phoenix Organs - Linux use in Church Organs 
    - Accessibility in Linux
        - IDRC Speaker 
        - Karen Luellen?
    - RaspberryPi - Chris Tyler
    - Open Source Program Officer - Evan
    - Open Street Map - Colen McGregor
    - ICANN - Gordon Chilcott
    - CIRA - 
    - ICANN Domain Resale Market - Moskavich
      - Digital Citiziship
    - Goverment Internet Restrictions - https://cippic.ca/ 
    - Talks from other technical groups
    - SNAPS, Containers vs Flatpacks
    - Webstream setup - Evan
    - Video and streaming creation in Linux - November
    - Talk to Chris Tyler about TFLOSS - Evan
    
**** Notes
    - Quorum not met

**** Confirm Date and Time of Next General Meeting
      Tuesday September 13th, at 7:30. Big Blue Button

**** Confirm Date and Time of next Board Meeting
      Wednesday September 21th, at 7:30. Big Blue Button



