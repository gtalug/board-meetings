---
layout: post
title: "Agenda"
published: true
---

* 21 October 2019 at 7:30pm

** Location
 - Starbucks, Davisville and Yonge
 - Zoom as backup

** Agenda Topics
 - Timing/Location of next Ops meeting
   - Look at Hacklab again
 - Upcoming Meetings
 - AGM Aftermath
 - ICANN matters
 - Write next month's agenda
** ToDo items
 - Bill [ ] :: Say something about 25 years of TLUG
 - Chris [ ] :: Visit Spadina/College CIBC to see requirements for Evan and Gord to become cosigners
 - Chris [ ] :: Ontario Form 1
 - Gord [X] :: Beg MORE for volunteers at next meeting, including board applicants
 - Evan [ ] :: See if Kelsey Merkeley can be drawn in for Lightning talk someday?
 - Chris [ ] :: Membership web page
 - Gord [ ] :: Approach folk at Ryerson to see who is still active with the student Linux user group
 - See what links are dead of user groups
 - Alex
   - email Nick to confirm GCC/LLVM December
   - then see if Hugh is keen on adding to that
   - if not, then we do Lightning talks
   - if so, then Lightning talks January
 - Gord [ ] :: Compose an email soliciting board members
 - Alex [X] :: How many videos have we published?  How heavily watched are they?
 - Alex [ ] :: Provide CoC report to Board to review, please forward the Board the address
