---
layout: post
title: "Agenda"
published: true
---

* 9 November 2020 at 7:00pm

** Location
 - Remote based on COVID-19
 - Jitsi

** Agenda Topics
 - The Passing of Chris Browne 
   - https://www.arbormemorial.ca/capital/obituaries/christopher-bruce-browne/57436/
 - Donation - via the obituary the family has asked for donations in lieu of flowers to go to Christian Service Brigade Canada.
 - Account signatories - we were about to add another person to the signatories. 
   - Scott says he still has signing authority.
 - Paypal - Does anyone else have control of this? I'm on the board of another local computer group that no longer has access to its Paypal account, and Paypal are very difficult to work with.
 - Ongoing payments
   - Review Finanical report given at last months AGM.
 - Accounts - these are probably rather well kept, but we may not know where.
 - Transition - we'll need another board member, and reallocate office-bearers.
   - Call a By-Election for the remainder of Chirs's Term.
 - Are there any SOPs in the technical infra?
   - Git Repos?
   - Server?
   - Email?

** TODO
