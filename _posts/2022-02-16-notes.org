---
layout: post
title: "Agenda"
published: true
---

* 16 Feb 2022 at 7:30pm

** Location

  Remote based on COVID-19 using LPI's Big Blue Button
  https://blue.lpi.org/b/eva-zjc-gjy-kgl


** Attendees
 - Present
    - Alan Heighway (board member)
    - Evan Leibovitch (board member)
    - Warren McPherson (board member)
    - Hugh Redelmeier (OPs Team)
    - Gordon Chilcott (OPs Team)
 
 - Absent
   - Alex Volkov (board member)
   - David Collyer-Brown (board member)
   - Scott Sullivan (OPs Team)

** Agenda Topics
  Speaker Search
  Confirm Date and Time of next board meeting
 
***  Items from the previous meetings Minutes.

  - [] Ensuring at least three people are sharing credentails via Lastpass - Alan.    
  - [] Publish the a list of services in LastPass - Alex
  - [] Update Corporate records / continunce / Form 1 - Evan, Scott, and Alan
  - [X] Contact David Collyer-Brown with regards to GTALUG board - Evan
  - [] Send out announce email about the usage of Discord and to pass out discord server around
  - [] Discuss Google utilities with Tech Team - Evan
  - [] Offline Discussion with Alex and Hugh on Google service
  - [X] Talk to Chris Tyler as a possible speaker - Alan
  - [X] Contact Scott re: Ryerson Contact (William Zereneh <zereneh@ryerson.ca>) - Evan 
  - [] Document the current notification process for general and board meetings - Alex
  - [X] Ask how the YRARC is updating the Discord server - Alan
  - [] Report on DNS and Hosting Status for next meeting - Alex
  - [] Document board position - Everyone
  - [] Check up on complition of of board position documents - Alan
  

*** New Business
    - Google for Non-profit access for board members

**** Febuary General Meeting

    - What can we do to ensure this doesn't happen again?

**** Speaker Search

    - Contacted Trevor Woerner - He would love to do a talk, awaiting response to follow-up message
    - Contacted IDRC - Tryting to arrange Date
    - Contacted Chris Tyler - No Response

**** To do to carry over to next month
  - [] Investigate user rights on BBB - Evan
  - [] Ensuring at least three people are sharing credentails via Lastpass - Alan.    
  - [] Publish the a list of services in LastPass - Alex
  - [] Update Corporate records / continunce / Form 1 - Evan, Scott, and Alan
  - [] Send out announce email about the usage of Discord and to pass out discord server around
  - [] Offline Discussion with Alex and Hugh on Google service
  - [] Discuss Google utilities with Tech Team - Evan, Alan, Ops Team
  - [] Document the current notification process for general and board meetings - Alex
  - [] Report on DNS and Hosting Status for next meeting - Alex
  - [] Document board position - Everyone
  - [] Check up on complition of of board position documents - Alan
  - [] Follow-up with IDRC again (Speaker Topic) - Alan
  - [] Open street map talk (Speaker Topic) - Alan

**** Confirm Date and Time of Next General Meeting
    
     Tuesday Mar 8th, at 7:30. Big Blue Button

**** Confirm Date and Time of next Board Meeting

    Wednesday Mar 16th, at 7:30. Big Blue Button

**** Adjourned
  Time: 8:52
      Time: 8:52
