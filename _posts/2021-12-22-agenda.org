---
layout: post
title: "Agenda"
published: true
---

* 22 Dec 2021 at 7:30pm

** Location

  Remote based on COVID-19 using LPI's Big Blue Button
  https://blue.lpi.org/b/eva-zjc-gjy-kgl

** Agenda Topics

  Items from the previous meetings Mintues.
  New Business
  Speaker Search
  Confirm Date and Time of next board meeting
 
***  Items from the previous meetings Minutes.

   - [] Ensuring at least three people are sharing credentails via Lastpass - Alan.    
   - [] Publish the a list of services in LastPass - Alex
   - [] Update Corporate records / continunce / Form 1 - Evan and Scott
   - [] David Collier-Brown has offered to be on the board - Evan
   - [] Resolve meeting software choices post - Scott
   - [] Investigate Discord Events - Evan
   - [] Discuss Google utilities with Tech Team - Evan
   - [] Check in to OWASP and task.to for possible speakers - Alan
   - [] Talk to Chris Tyler as a possible speaker
   - [] Meeting location nailed down a week before meeting - Warren
  
*** New Business

**** December General Meeting

       - Meeting didn't happen: what went wrong?
       - What can we do to ensure this doesn't happen again?

**** Speaker Search

      -  Chris Sullivan - Direction finding using Dopler with Raspberry Pi, Sound Card, Other Hardware, and Python (Title Pending)

**** Confirm Date and Time of Next General Meeting
    
      Monday Jan 11th, at 7:30. Google Meet

**** Confirm Date and Time of next Board Meeting
      Wednesday Jan 19th, at 7:30. Google Meet
