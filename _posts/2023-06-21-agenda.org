---
layout: post
title: "Agenda"
published: true
---

* 21 June 2023 at 7:30pm

** Location

 BigBLueButton: https://blue.lpi.org/b/eva-zjc-gjy-kgl

** Agenda Topics
  Items from the previous meetings Minutes.
  New Business
  Speakers / Speaker Search
  Confirm Date and Time of next board meeting

  *** Items from the previous meetings Minutes.
- [] Create a linode account for GTALUG and facilitate the movement of the GTALUG website to the new account with the help of Alex. (Hugh)
- [] Check up on completion of board position documents - (Alan)
- [] Update Corporate records / continuance / Form 1 - (Evan and Alan)
- [] Check up on completion of of board position documents - (Alan)
- [] Fill Vacant Positions on board (Board)
- [] Who Has Our Twitter Credetntials (Alan)
- [] Membership section on website (Alan)
- [] Talk to Chris Tyler about TFLOSS - (Evan)
- [X] Reach out to Myles about a talk (Alan)
- [X] Doodle pole to go out about a meeting of the minds (Evan)
- [] Draft letter to general membership about merger with CLUE (Alan)

*** New Business

*** Speaker Search (Topics and possible presenters, and months)
    - Video and streaming creation in Linux - Evan and Alan - July
    - Rocky Linux - Brian Clemens and team - September 12th
    - Mozilla Update - October - (Mike Hoye)
    - Mastodon 2 - Architect of the Organization -  
    - Mastodon 3 - Technical aspects of starting a node -
    - Stability A.I.
    - Open Source Inntelligence
    - Multithreading in C++ and heterogeneous computing - Michael Wong
    - Open Street Map - Colen McGregor
    - ICANN - Gordon Chilcott
    - Google Ad Words - Use and value to the group
    - Don from Phoenix Organs - Linux on Rasp Pi use in Church Organs 
    - Accessibility in Linux/
        - IDRC Speaker
        - Karen Luellen?
    - Open Source Program Officer - Evan
    - CIRA - 
    - ICANN Domain Resale Market - Moskavich
      - Digital Citizenship
    - Government Internet Restrictions - https://cippic.ca/ 
    - Talks from other technical groups
    - SNAPS, Containers vs Flatpacks
    - Talk to Chris Tyler about TFLOSS - Evan
    - Daniel Micay (lead developer of GrapheneOS
    - Speaker from TCOWS
    - Steam - Valve - Games on Linux

**** Confirm Date and Time of Next General Meeting
      Tuesday July 11th, at 7:30. Big Blue Button

**** Confirm Date and Time of next Board Meeting
      Wednesday July 19th, at 7:30. 
