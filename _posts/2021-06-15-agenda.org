---
layout: post
title: "Agenda"
published: true
---

* 16 June 2021 at 7:30pm

** Location
 - Remote based on COVID-19
 - Jitsi

** Agenda Topics
 - Speaker Search
 - Review of communication breakdown around scheduling of May's meeting

** TODO
 - [] Secretray Handbook - Scott
 - [] Adjuste Speaker Coordinator alias on mail server to point at Alan.
 - [] Add Hugh to pengin server. - Alex
 - [] Remove Stewart from Board Mailing list - Alex and Hugh
 - [] Ensuring at least three people are sharing credentails via Lastpass - Alan.
 - [] Publish the a list of services in LastPass - Alex
 - [] Write Reimbursements check for Alex - Scott
