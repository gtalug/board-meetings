---
layout: post
title: "Notes"
published: true
---

* 21 October 2019 at 7:30pm

** Location
 - Starbucks, Davisville and Yonge

** Attendees
- TBD
    - Evan Liebovich
    - Bill Thanis
    - David Collier Brown
- Present
    - Christopher Browne (board member)
    - Gordon Chillcott
    - Syed Nasir (board member)
    - Warren McPherson (board member)
    - Jeff Pikul (board member)
- Absent
    - Alex Volkov (board member)
    - Alan Heighway (board member)
** Topics
*** Next Ops Meeting
  - November Date would be Nov 18th
  - location :: Starbucks Davisville
  - Researching other ideas, such as TPL
  - perhaps at a public library?
    - Reference Library?
    - Yonge and Lawrence?
*** Banking thing
  - need to add a cosigner to replace Scott
*** Upcoming Meetings
**** October
  - Mike Hoye on Firefox
  - AGM
**** November
  - Tomas Babej (tomas@tbabej.com) has agreed to give about 60 minute
    presentation on taskwarrior on November 12 meeting.
**** December
  - Nick, proposing a GCC/LLVM Q and A
  - Alex will be away; Elaine/Myles might be able to handle video
**** January
  - Tomas Bibaj on on Yubikey and PAM
  - Mike Kalles had something, couldn't make August
  - Debian talk from Sergio
  - Kubernetes talk from Chris
  - Universal Access talk from Gord
**** February
  - Amateur Radio with Alan
**** Generic Topics
  - Kelsey Merkley on Ontario Government and OSS
  - Database Panel - state of OSS databases
    - Chris on PG
  - Consider notion of AMA (Ask Me Anything) later
    - Thus, we invite someone to stand up front and field questions
  - Glenn McKnight on Mesh Networks (assuming good response on Alan's piece on amateur radio)
  - Series on privacy topics (Warren as a key person)
  - Mailing list moderation and CoC
**** Meta topics
 - Need MC to introduce things, /not Alex/
   - Ought to rotate it
*** ICANN
**** CPWG
 - Reorganizing, documenting what CPWG does and how
 - Third Accountability and Transparency Review ATRT3 is starting
   - ALAC might ask for CPWG to help
**** WHOIS/Registrant DB
 - Ongoing
 - Sundry topics
   - responsibilities for data accuracy
   - to whom queries are sent (ICANN, Registrars, Registries)
   - automated request processing
**** Next gTLD round
 - Discussion continuing
 - Objections issue
   - likeliest an applicant problem
   - objection process is expensive
   - At Large requires funding to serve this
   - Appeals has arisen as a topic, along with At Large ability/right to file appeals
**** Geographic Names
 - likely to be discussed in Montreal ICANN in November
 - survey involving various scenarios around such TLDs
   - .hamilton might have 16 towns/cities plus a kitchen appliance company
   - who wins?
 - survey coming to GTALUG Board
**** Evolving Multistakeholder Model
 - CPWG presented comment on our view of priorities on major topics
 - comment tried to emphasize things that seem important in effects on Internet end-users
 - concerns about emerging Policy Development Process v3 and hints that inclusion in process may be restricted
**** Other Items
 - DNS abuse has popped up again
 - GNSO produced a pretty good paper describing this
 - Universal Acceptance people looking to push message and we'll be asked to help
*** AGM Followup
 - for next year, we should approach Hugh as a potential returning officer
 - need to elect crucial officer positions
   - President :: Has been Alex
   - Secretary/Treasurer :: Has been Chris
 - other duties
   - MC for meetings :: has been Evan, but with some help from Alex and Chris
   - Code of Conduct evaluation :: Rules need people to verify what is being done about the rules
   - Speaker contact :: has mostly been Alex
   - Systems :: Mostly things are done by Alex; it would be nice to spread this out a bit more
     - Generating announcements
     - Checking that backups are taken
     - It would be nice to "lint" some things such as the list of other local user groups
     - Should spruce up membership and volunteer web pages and such 
     - We should add a moderator for the mailing lists
*** Membership
 - need to actively do some recruitment of general membership
 - need a web page describing what we want said
 - need to do some active recruiting for Operations
*** ToDo items 
 - Bill [ ] :: Say something about 25 years of TLUG
 - Chris [ ] :: Visit Spadina/College CIBC to see requirements for Evan and Gord to become cosigners
 - Chris [ ] :: Ontario Form 1
 - Gord [ ] :: Beg MORE for volunteers at next meeting, including board applicants
 - Evan [ ] :: See if Kelsey Merkeley can be drawn in for Lightning talk in December/January?
 - Chris [ ] :: Compose an announcement about membership
   - Do up a MindMap document that spreads across the various things
     - Participation
       - Mailing list
       - Speaking :: Helps us have things at meetings
       - Membership :: wee formality
       - Social
         - Dinner before
         - Beer after
         - Annual picnic
     - Volunteering :: Helping us make the meetings work
       - ICANN
       - Operations
       - Board
       - Video recording
       - Technical support (running the server)
         - need more automation
 - Chris [ ] :: Membership web page
 - Gord [ ] :: Approach folk at Ryerson to see who is still active with the student Linux user group
 - See what links are dead of user groups
 - Alex
   - email Nick to confirm GCC/LLVM December
   - then see if Hugh is keen on adding to that
   - if not, then we do Lightning talks
   - if so, then Lightning talks January
 - Gord [ ] :: Compose an email soliciting board members
 - Alex [ ] :: How many videos have we published?  How heavily watched are they?
 - Alex [ ] :: Provide CoC report to Board to review, please forward the Board the address
 - Alex [ ] :: update Ops mailing list to have Warren and Syed added
 - Chris [ ] :: Check hours and stuff at Toronto Reference Library
 - Gord [ ] :: Looking into a Zoom account to replace Evan's
 - Warren [ ] :: Check with a couple TPL locations
 - Chris [ ] :: Burn some more GTALUG cards
