---
layout: post
title: "Agenda"
published: true
---

* 19 April 2023 at 7:30pm

** Location

 Meeting at 'The Pilot' in Person

** Agenda Topics
  Items from the previous meetings Minutes.
  New Business
  Speakers / Speaker Search
  Confirm Date and Time of next board meeting
 
*** Items from the previous meetings Minutes.
- [] Update Corporate records / continuance / Form 1 - Evan and Alan
- [] Check up on completion of board position documents - Alan
- [] Update Corporate records / continuance / Form 1 - Evan and Alan
- [] Check up on completion of of board position documents - Alan
- [] Doodle pole to go out about a meeting of the minds. - Evan
- [] Fill Vacant Positions on board
- [] Create a linode account for GTALUG and facilitate the movement of the GTALUG website to the new account with the help of Alex. -Huge

*** Old Business
    - Domain registration up for renewal. 
        - Apparently, we also have gtalug.info, the board has chosen to drop this and only keep gtalug.org.
        - Alex will facilitate the renewal and invoice the group.
    - Alex is currently hosting the GTALUG website, he has suggested we get our own LiNode account and have the site transfered
    
*** Speaker Search (Topics and possible presenters, and months)
    - Open AI - April - Marcel(sp?)
    - Video and streaming creation in Linux - Evan and Alan - May?
    - Mastodon 2 - Architect of the Organization -  
    - Stability A.I.
    - Mastodon 3 - Technical aspects of starting a node -   - June
    - Multithreading in C++ and heterogeneous computing - Michael Wong
    - Open Street Map - Colen McGregor
    - ICANN - Gordon Chilcott
    - Google Ad Words - Use and value to the group
    - Don from Phoenix Organs - Linux on Rasp Pi use in Church Organs 
    - Accessibility in Linux
        - IDRC Speaker
        - Karen Luellen?
    - Open Source Program Officer - Evan
    - CIRA - 
    - ICANN Domain Resale Market - Moskavich
      - Digital Citizenship
    - Government Internet Restrictions - https://cippic.ca/ 
    - Talks from other technical groups
    - SNAPS, Containers vs Flatpacks
    - Talk to Chris Tyler about TFLOSS - Evan
    - Daniel Micay (lead developer of GrapheneOS
    - Speaker from TCOWS
    - Steam - Valve - Games on Linux

**** Confirm Date and Time of Next General Meeting
      Tuesday May 9th, at 7:30. Big Blue Button

**** Confirm Date and Time of next Board Meeting
      Wednesday May 16th, at 7:30. 