---
layout: post
title: "Notes"
published: true
---

* 18 November 2019 at 7:30pm

** Location
 - Starbucks, Davisville and Yonge

** Attendees
- TBD
    - Evan Liebovich
    - David Collier Brown
    - Warren McPherson (board member)
    - Jeff Pikul (board member)
- Present
    - Bill Thanis
    - Christopher Browne (board member)
    - Gordon Chillcott
    - Syed Nasir (board member)
    - Alex Volkov (board member)
    - Alan Heighway (board member)
- Absent
** Topics
*** Next Ops Meeting
  - December Date would be Dec 16
  - Ideas include
    - TPL, contacts include Giles, Stewart, Laetitia
    - City Hall, Metro Hall, perhaps via DCB
*** Banking thing
  - need to add a cosigner to replace Scott
*** Upcoming Meetings
**** November
  - Tomas Babej (tomas@tbabej.com) on TaskWarrior
**** December
  - Nick, proposing a GCC/LLVM Q and A
  - Alex will be away; Elaine/Myles might be able to handle video
**** January
  - Lightning talks
  - Tomas Bibaj on on Yubikey and PAM
  - Mike Kalles had something, couldn't make August
  - Debian talk from Sergio
  - Kubernetes talk from Chris
  - Universal Access talk from Gord
**** February
  - Amateur Radio with Alan
**** March
  - Mike Kalle's talk?
  - Lightning strikes yet again?
**** April
  - Q&A, maybe?
  - Or state of OSS DBs panel?
**** Generic Topics
  - Database Panel - state of OSS databases
    - Chris on PG
  - Consider notion of AMA (Ask Me Anything) later
    - Thus, we invite someone to stand up front and field questions
  - Glenn McKnight on Mesh Networks (assuming good response on Alan's piece on amateur radio)
  - Series on privacy topics (Warren as a key person)
  - Mailing list moderation and CoC
**** Meta topics
 - MC was Warren at the last meeting
   - Ought to rotate it
*** ICANN
**** ICANN66
 - AGM, Nov 2-7
**** DNS Abuse
 - product of Competition, COnsumer Trust and Consumer Choice Review
 - tried to define "DNS Abuse" and ICANN's role
 - some matters are in Registrar and Registry agreements
 - DAAR exists, provides some useful information, and was discussed
 - Some reports obtained, to be forwarded to GTALUG board
 - concern that if Domain community does not get a handle on this, governments will produce legislation instead
**** Expedited Policy Development Process on Registrant DB
 - plan for initial report in January 2020 (or June)
**** Universal Access
 - dedicated sessions and discussions
 - Gord would like to present a slide presentation on this at a meeting
 - ICANN making a point to ask At Large membership to help make it more of a public concern
 - others feel UA is outside ICANN's remit
**** Evolving the Multi-Stakeholder Model
 - comment period showed some major concerns
   - prioritization as some projects are external (IANA Transition, GDPR)
   - some pressures are created due to many internal reviews
   - Culture and Silos
   - Issue complexity
   - need to carefully scope work, clearly define roles and responsibilities
 - next step is to assign work to parts of ICANN that can handle them
**** Subsequent Procedures  
 - new gTLD round
*** AGM Followup
 - for next year, we should approach Hugh as a potential returning officer
 - need to elect crucial officer positions
   - President :: has been Alex, and he continues to be interested
     - consensus achieved for him to continue
   - Secretary/Treasurer :: Has been Chris
     - consensus achieved for him to continue
 - other duties
   - MC for meetings :: has been Evan, but with some help from Alex and Chris
     - Warren did this at Nov meeting
   - Code of Conduct evaluation :: Rules need people to verify what is being done about the rules
   - Speaker contact :: has mostly been Alex
   - Systems :: Mostly things are done by Alex; it would be nice to spread this out a bit more
     - Generating announcements
     - Checking that backups are taken
     - It would be nice to "lint" some things such as the list of other local user groups
     - Should spruce up membership and volunteer web pages and such 
     - We should add a moderator for the mailing lists
*** Membership
 - need to actively do some recruitment of general membership
 - need a web page describing what we want said
 - need to do some active recruiting for Operations
*** ToDo items 
 - Chris - capture the URL with German LUG cards
 - Warren to contact TPL about meeting room
 - Gord to poke a councillor to get meeting room
 - Gord and Chris go to the bank
 - Alex, poke people on talks
   - Nick, for December
   - Tomas, for Yubikey/PAM
   - Mike Kalles on something from August
   - Sergio on Debian
   - Chris on Kubernetes
   - Miles on something
   - Jill (that did DeepFakes in August)
   - Alex should contact Alex about a talk Alex would do on Spam filtering for mailing lists
 - Alex, present CoC proposals at Dec/Jan board meeting
 - Alan, set up GitLab account, we'll add him to the GTALUG stuff, send to Alex
 - Alan, "lint" the Other Local Users Group list
 - Alex, add the new members to the Ops list
 - DONE Chris :: Future Publicity - German Linux Card https://twitter.com/lughannover/status/1193985622521896961
   - things they have we do not
     - QR barcode
     - link to twitter account
     - email address
   - seems like we have more or less enough stuff
 - Chris :: IndieWeb alternative to Meetup.com https://www.jvt.me/posts/2019/11/14/ditch-event-platforms-indieweb/
   - https://marcusnoble.co.uk/2019-10-21-meetup-alternatives/
   - Already live, somewhat...  https://gettogether.community/events/3158/lightning-talks/
     - code available https://github.com/GetTogetherComm/GetTogether
     - Django, Python3
   - https://www.attendize.com/index.html and https://github.com/Attendize/Attendize
