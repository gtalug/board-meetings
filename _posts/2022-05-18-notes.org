---
layout: post
title: "notes"
published: true
---

* 18 May 2022 at 7:30pm

** Location

  Remote based on COVID-19 using LPI's Big Blue Button
  https://blue.lpi.org/b/eva-zjc-gjy-kgl


** Attendees
 - Present
    - Alan Heighway (board member)
    - Evan Leibovitch (board member)
    - Hugh Redelmeier (Ops Team)
    - Gordon Chilcott (Ops Team)
     
 - Absent
   - Warren McPherson (board member)
   - Alex Volkov (board member)
   - David Collyer-Brown (board member)
   - Scott Sullivan (Ops Team)

** Agenda Topics
   Items from the previous meetings Mintues.
   Speaker Search
   Confirm Date and Time of next board meeting
 
**** To do
  - [] Ensuring at least three people are sharing credentials via Lastpass - Alan.    
  - [] Publish the a list of services in LastPass - Alex
  - [] Update Corporate records / continuance / Form 1 - Evan and Alan
  - [] Offline Discussion with Alex and Hugh on Google service
  - [] Discuss Google utilities with Tech Team - Evan, Alan, Ops Team
  - [] Document the current notification process for general and board meetings - Alex
  - [] Report on DNS and Hosting Status for next meeting - Alex
  - [] Document board position - Ops Tasks - Everyone
  - [] Check up on completion of of board position documents - Alan
  - [] Follow-up with IDRC again (Speaker Topic) - Alan
  - [] Open street map talk (Speaker Topic) - Alan
  - [] Follow up with tech soup on Google ad words / analytics - Evan
  - [] Even and Alan to work on Google for Non-profit
  - [] Follow up with Alex to arrange knowledge transfer - Alan
  
*** Meeting Notes

    - Quorum was not met
    - Software Freedom Day - September 17, 2022
        - Ask for intersted parties at June meeting.
        - Install fest?
        - Ask me anything
  
*** New Business
    - June Meeting
        - What would a Linux open house look like today?

**** Speaker Search
  - Open Source on the Water - Marine Navigtion via Raspberry Pi - Colen McGregor - September Speaker
  - Futur Topic - Warren - July?
  - Google Ad Words - Use and value to the group - August?
  - Don Anderson from Phoenix Organs - Linux use in Church Organs - Future Speaker?
  - IDRC Speaker
  - RaspberryPi - Chris Tyler
  - Open Source Program Officer - Evan
  - Open Street Map - Colen McGregor
  - ICANN - Gordon Chilcott
  - CIRA - 
  - ICANN Domain Resale Market - Moskavich
  - Digital Citiziship
  - Goverment Internet Restrictions - https://cippic.ca/ - Michael Geist (Reference)
  - Talks from other technical groups

**** To do to carry over to next month
  - [] Ensuring at least three people are sharing credentails via Lastpass - Alan.    
  - [] Publish the a list of services in LastPass - Alex
  - [] Update Corporate records / continunce / Form 1 - Evan and Alan
  - [] Offline Discussion with Alex and Hugh on Google service
  - [] Create Google accounts for the board members - Evan
  - [] Document the current notification process for general and board meetings - Alex
  - [] Report on DNS and Hosting Status for next meeting - Alex
  - [] Document board position - Ops Tasks - Everyone
  - [] Check up on complition of of board position documents - Alan
  - [] Follow-up with IDRC again (Speaker Topic) - Alan
  - [] Even and Alan to work on Goolgle for Non-profit
  - [] CIRA Speaker
  
**** Confirm Date and Time of Next General Meeting
    
     Tuesday June 14th, at 7:30. Big Blue Button

**** Confirm Date and Time of next Board Meeting

    Wednesday June 22th, at 7:30. Big Blue Button

****
    Adjourned 8:50



