---
layout: post
title: "Notes"
published: true
---

* 15 June 2021 at 8:15pm

Meeting started late due to techinical delays and late arrivals.

** Location
 - Remote based on COVID-19
 - Jitsi

** Attendees
- Present
    - Alan Heighway (board member)
    - Scott Sullivan (board member)
    - Alex Volkov (board member)
    - Warren McPherson (board member)

** Agenda Topics
 - Speaker Search
 - Review of communication breakdown around scheduling of May's meeting

** Items from the previous meetings Mintues.
 - [] Secretray Handbook - Scott
 - [x] Adjuste Speaker Coordinator alias on mail server to point at Alan.
 - [x] Add Hugh to pengin server. - Alex
 - [x] Remove Stewart from Board Mailing list - Alex and Hugh
 - [] Ensuring at least three people are sharing credentails via Lastpass - Alan.
 - [] Publish the a list of services in LastPass - Alex
 - [] Write Reimbursements check for Alex - Scott

** Meeting Mintues

*** Speaker Search
 - No one had leads for this month. Warren has a few for later months.
 - Round Table Q and A defaulted to for July.

*** Review of communication breakdown around scheduling of May's meeting.
 - Zoom meeting URL had been prepared but not communicated.
 - Meeting announce was sent last minute out without confirming details, but 'got the job done'.
 - Agreed to migrate duty to Scott as Alex is ramping down.

*** Date and Time of Next meeting.

 - Wed July 21st, 7:30 pm
 - Alan now has a regular conflict on the board meeting Tuesdays. Consensus was reached to default to wednesdays.

** TODO
 - [] Secretray Handbook - Scott
 - [] Ensuring at least three people are sharing credentails via Lastpass - Alan.
 - [] Publish the a list of services in LastPass - Alex
 - [] Write Reimbursements check for Alex - Scott
 - [] On-board Scott on current meeting announce process. - Alex
