---
layout: post
title: "Notes"
published: false
---

* 25 February 2019 at 7:30pm

** Location

- Zoom

** Attendees
- TBD
    - Evan Liebovich (board member)
    - David Collier Brown
    - Myles Braithwaite
- Present
    - Scott Sullivan (board member)
    - Christopher Browne (board member)
    - Gordon Chillcott (board member)
    - Bill Thanis
    - Alex Volkov (board member)
- Absent
  
** Topics
*** Next Ops Meeting
  - Hacklab continues to be unavailable
  - Let's Zoom for now...
    - Natural March date is March 18th

*** Upcoming Meetings

**** February
  - Chris Tyler on TBD
    - Alas, snowed out, so deferred to March

**** March
  - Chris Tyler on FPGAs

**** April
  - Python theme evening?
    - Myles may have a candidate for some Pythonesque material, TBD come January
    - Plenty of time
  - See if Lennart and/or Hugh could have hardware war stories

**** May
  - Q&A?
  - Stewart Russell on his new community org

**** Generic Topics
  - Want a talk on Laptop support
    - Lennart would be a fine candidate to try to draw on
  - Database Panel - state of OSS databases
    - Peter on MySQL, MariaDB
    - Chris on PG
    
*** ICANN material from Gord
  - Atlas III
    - Now have dates for ICANN 66, Nov 2-7
    - Some process for setting program and selecting the 60, requirements might involve attendance to ICANN Learn classes
  - Consolidated Policy Working Group
    - WHOIS Policy Development reached deadline
    - At Large submitted comments
    - Some issues not at consensus
    - Phase I is "finished", Phase II to begin
      - Phase I :: Policy
      - Phase II :: Implementation strategy
    - GTLD next round
      - New news
    - Geographic name TLDs
      - No more progress; may be discussed at ICANN @ Kobe
  - NARALO
    - Major item was ATLAS III
    - Canadian conference on governance at Hart House on Wednesday
  - Technology Task force
    - Tech staff were available for the call
    - Problems with email Spanish/English translator and budget for maintenance
    - Problems with Confluence
    - Open question on when ICANN will use latest Adobe Connect
    - Open question on progress of Zoom "pilot"
    - Some mention of Enterprise Slack
    - Budget constraints, particularly surrounding email translator maintenance
  - Gord will be out at ICANN Japan on March 9-14th
*** Meetup.com
 - Followup needful; we approved $60 USD for budget to Alex
 - Probably need to pay him?
 - Initial results?
*** Code of Conduct
 - Update wording to apply to all GTALUG functions
   - Pre-meeting Dinner
   - Meeting itself
   - Post-meeting beer
   - Special Events, BBQ
 - We already mention it quite prominently; do we have a grey area?
   - Dinner is not a GTALUG event per se, though it is mentioned
 - The section entitled Violating the Code of Conduct seems to cover this decently
*** Infrastructure
 - Linode is working on having Canada-based servers, but not ready, so that is moot
 - Changing service providers is a pain in the neck
 - Hetzner has been treating Alex well...
*** Write next month's agenda
 - Timing/Location of next Ops meeting
 - Upcoming Meetings
 - ICANN material from Gord
 - Write next month's agenda.

*** ToDo items
 - Alex :: Look up Hardware items per Hugh and/or Lennart
 - Alex to contact Stewart Russell to see if he has anything with his new place
